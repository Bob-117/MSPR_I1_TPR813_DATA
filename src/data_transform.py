import logging
import pprint

from tabulate import tabulate


########################################################################################################################
# DATA ANALYSIS
########################################################################################################################
def print_dataset(_df, title=None):
    if title:
        print(f'************** {title}')
    # print(tabulate(_df[0:10], headers='keys', tablefmt='psql'))
    print(tabulate(_df, headers='keys', tablefmt='psql'))


def print_dataset_prefix(_df, col_name):
    cols = [c for c in _df.columns if col_name in c]
    print(col_name)
    print_dataset(_df[['Name'] + cols])


def print_stats_dict(_dict, _title):
    """
    huho
    """
    print(f'----- {_title} -----')
    # pprint.pprint(_dict)
    for k, v in _dict.items():
        print(f'{k} : {v}')


def __advanced_stats(_dataframe):
    _nb_col_by_type = nb_col_by_type(_dataframe=_dataframe)
    _col_name_by_type = col_name_by_type(_dataframe=_dataframe, __nb_col_by_type=_nb_col_by_type)
    unique_values = unique_values_by_col(_dataframe=_dataframe, __col_name_by_type=_col_name_by_type)

    return {
        'type': _nb_col_by_type,
        'name': _col_name_by_type,
        'unique': unique_values,
    }


def nb_col_for_type(_dataframe, _type):
    """
    Return the number of columns for one given type
    """
    s = 0
    for col in _dataframe.columns:
        s += (_type == _dataframe[col].dtype)
    return s


def all_col_of_one_type(_dataframe, _type):
    """
    Return all columns of a given type as a sub dataframe
    """
    return _dataframe.select_dtypes(include=_type)


def nb_col_by_type(_dataframe):
    """
    Return dict such as :

    bool : 1
    float64 : 2
    object : 19
    int64 : 8

    :param _dataframe:
    :return:
    """
    unic_col_types = set(_dataframe.dtypes)
    __nb_col_by_type = {
        f'{_type}': nb_col_for_type(_dataframe, _type)
        for _type in unic_col_types
    }
    # logging.debug(f'nb_col_by_type : {type(__nb_col_by_type)}')
    return __nb_col_by_type


def col_name_by_type(_dataframe, __nb_col_by_type):
    """
    Return dict such as :

    bool : ['Survived']
    float64 : ['Public Favor', 'Mental Resilience']
    object : ['Name', 'Origin', 'Category', 'Special Skills',]
    int64 : ['Unnamed: 0', 'Age', 'Birth Year',]

    :param _dataframe:
    :param __nb_col_by_type:
    :return:
    """
    cols_by_type = {
        f'{t}': [c for c in all_col_of_one_type(_dataframe, t)]
        for t, _ in __nb_col_by_type.items()
    }
    # logging.debug(f'col_name_by_type : {type(cols_by_type)}')
    return cols_by_type


def unique_values_by_col(_dataframe, __col_name_by_type, only_type='object', max_value=20):
    """
    Return dict such as :

    Name : 499
    Origin : ['Gaul' 'Greece' 'Rome' 'Thrace' 'Numidia' 'Germania']
    Category : ['Hoplomachus' 'Provocator' 'Retiarius' 'Secutor' 'Murmillo' 'Thraex']
    Special Skills : ['Novice' 'Agility' 'Endurance' 'Tactics' 'Speed' 'Strength']
    Weapon of Choice : ['Spear' 'Dagger' 'Net' 'Gladius (Sword)' 'Sica (Curved Sword)' 'Trident']
    Patron Wealth : ['Low' 'High' 'Medium']

    # Helps us find features to use One Hot Encode on
    :param _dataframe:
    :param __col_name_by_type:
    :param only_type:
    :param max_value:
    :return:
    """
    unique_values = {
        f'{c}': [list((_dataframe[c].unique())) if len(_dataframe[c].unique()) < max_value else len(_dataframe[c].unique())]
        for c in __col_name_by_type.get(only_type)
    }
    # logging.debug(f'unique values by col : {type(unique_values)}')
    # print('******************************************************')
    # print(f'unique values by col : {unique_values}')
    # print('******************************************************')

    return unique_values


## EXECUTION
def print_advanced_stats(_dataframe):
    """
    ...
    """
    print('***** ADVANCED STATS *****')
    # Basics
    num_rows, num_cols = _dataframe.shape
    print(f"{num_rows} Rows, {num_cols} Cols ({list(_dataframe.columns)}")
    # Number of column by type
    _nb_col_by_type = nb_col_by_type(_dataframe=_dataframe)
    print_stats_dict(_dict=_nb_col_by_type, _title='Nb col by type')

    # Column name by type
    _col_name_by_type = col_name_by_type(_dataframe=_dataframe, __nb_col_by_type=_nb_col_by_type)
    print_stats_dict(_dict=_col_name_by_type, _title='Cols name by type')

    # Unique values by column (object type only & 20 max by default)
    unique_values = unique_values_by_col(_dataframe=_dataframe, __col_name_by_type=_col_name_by_type)
    a = [
        c for c in _dataframe.columns if c not in _col_name_by_type.get('object')
    ]
    print_stats_dict(_dict=unique_values, _title='Unique values for object type column (limit 20)')
    print(f'Ignored col for unique values : {a}')
