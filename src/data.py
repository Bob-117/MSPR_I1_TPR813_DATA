class Filter:
    def __init__(self, col_name, value):
        self.col_name = col_name
        self.value = value

    def exec(self, data):
        return data[self.col_name] == self.value



    # print(df)
    #
    # filtered_df = df[df['Libellé de la commune'] == 'Rennes']
    #
    # print(filtered_df)
    # filtered_df.to_csv('rennes.csv')
    # city_filter = Filter(col_name='Libellé de la commune', value='Rennes')
    # rennes_df = df.where(city_filter.exec(df), inplace=True)
    # print(rennes_df)



# def __file_type(file_name):
#     mime = magic.Magic(mime=True)
#     t = mime.from_file(file_name)
#     print(f'Type found for file {file_name} : {t}')
#     return t


# def create_sub_dataset(_file_in, _len=500):
#     """
#     Development purpose
#     """
#     logging.debug('create_sub_dataset')
#     _content = pd.read_csv(locate_csv_file(_file_in))
#     _out = os.path.join(os.path.dirname(os.path.abspath(__file__)), BASE_DATA_FILE_PATH, f'gladiator_{_len}.csv')
#     _content[0: _len].to_csv(os.path.join(BASE_DATA_FILE_PATH, _out))
#
#
# def read_data(_csv_file):
#     """
#     Read a csv file
#     Return the content as pandas dataframe
#     """
#     logging.info(f'Reading {_csv_file}')
#     _data = pd.read_csv(_csv_file)
#     return _data
#
#
# def locate_csv_file(_file_name):
#     logging.debug('locate_csv_file')
#     _file = os.path.join(os.path.dirname(os.path.abspath(__file__)), BASE_DATA_FILE_PATH, _file_name)
#
#     if not os.path.exists(_file):
#         logging.error('file not found')
#         raise FileNotFoundError(f"The file at {_file} does not exist.")
#
#     if not _file.lower().endswith('.csv'):  # todo mimetype
#         logging.error('csv?')
#         raise ValueError(f"The file at {_file} is not a CSV file.")
#     logging.info('File found')
#     return _file


