"""
Transforms the csv file resultats_elections_cumul_presidentielles_rennes.csv, downloaded from :
https://data.rennesmetropole.fr/api/explore/v2.1/catalog/datasets/resultats_elections_cumul_presidentielles_rennes/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B

The goal is to extract every candidats names for each election/turn, and put them in one column.
"""
import logging
import os
from asyncio import run

import numpy as np
import pandas as pd
from google.auth.transport import requests

from src.political import PROJECT_DIR, conn

file_to_transform = "resultats_elections_cumul_presidentielles_rennes.csv"

columns_to_get = [
    "CODE_ELECTION", "NUMERO_TOUR", "DATE_ELECTION", "CODE_COMMUNE", "LIBELLE_COMMUNE",
    "NIVEAU_DETAIL", "NUMERO_LIEU", "NOM_LIEU", "ADRESSE_LIEU", "NUM_CENTRE",
    "NUMERO_CANTON", "NOM_CANTON", "NUMERO_CIRCONSCRIPTION", "NOM_CIRCONSCRIPTION", "NB_INSCRITS",
    "NB_EMARGEMENTS", "NB_BULLETINS", "NB_BLANC", "NB_NULS", "NB_EXPRIMES", "POURCENTAGE_PARTICIPATION"
]

class_keys = ['CANDIDAT', 'POURCENTAGE', 'NB_VOIX']


def group_candidats_to_one_column(line: pd.Series):
    """
    Group candidats to one column, with columns <columns_to_get> + <class_keys>
    """
    # scan columns names to determine the number of candidats
    nombre_candidats = len([col for col in line.index if col.startswith("CANDIDAT")] or [])
    for i in range(1, nombre_candidats + 1):
        candidat = line[f"{class_keys[0]}_{i}"]
        if candidat is None or pd.isna(candidat):
            continue

        pct_votes = line[f"{class_keys[1]}_{i}"]
        yield pd.Series({
            **{col: line[col] for col in columns_to_get},
            class_keys[0]: candidat,
            class_keys[1]: float(pct_votes.replace(',', '.')) if isinstance(pct_votes, str) else pct_votes,
            class_keys[2]: int(line[f"{class_keys[2]}_{i}"]) if line[f"{class_keys[2]}_{i}"] is not pd.NA else np.nan,
        })
    return


def flatten_candidats(df):
    """
    File_Name must not contain file extension
    """
    flattent_df = pd.DataFrame()
    for index, line in df.iterrows():
        grouped = group_candidats_to_one_column(line)
        for _line in grouped:
            flattent_df = flattent_df._append(_line, ignore_index=True)
    return flattent_df


def to_db(df, name):
    df.to_sql(name, conn, if_exists="replace")


async def save_file(response, __save_name: str) -> None:
    with open(os.path.join(PROJECT_DIR, 'data', 'extracted', __save_name), 'wb') as file:
        file.write(response.content)
        file.close()


def get_document(__url: str, __save_name: str=None) -> None:
    if os.path.exists(os.path.join(PROJECT_DIR, 'data', 'extracted', f"{__save_name}.csv")):
        logging.info("File %s already exists, skipping fetch", __save_name)
        return
    logging.info("Fetching %s", __url)

    async def get():
        with requests.Request().session.get(__url) as response:
            if response.status_code != 200:
                raise Exception(f"Error {response.status_code}")
            if __save_name is not None:
                await save_file(response, __save_name)
            return response
    return run(get())


if __name__ == '__main__':
    url = ("https://data.rennesmetropole.fr/api/explore/v2.1/catalog/datasets/"
           "resultats_elections_cumul_presidentielles_rennes/exports/csv"
           "?lang=fr"
           "&timezone=Europe%2FBerlin"
           "&use_labels=true"
           "&delimiter=%3B")
    save_name = "resultats_elections_rennes_metropole_flatten.csv"
    get_document(url, save_name)
    df = pd.read_csv(os.path.join(PROJECT_DIR, 'data', 'extracted', f"{file_to_transform}.csv"), sep=';')
    flatten = flatten_candidats(df)
    to_db(flatten, "resultats_elections_rennes_metropole_flatten")
