import os
import logging
import sqlite3

import colorama


colorama.init()
logging.basicConfig(level=logging.INFO)
__logger = logging.getLogger(__file__.replace('\\', '/').split('/')[-1].split('.')[0])

PROJECT_DIR = os.path.abspath(__file__).split('src')[0]
conn = sqlite3.Connection(os.path.join(PROJECT_DIR, 'data', 'politics.db'))
