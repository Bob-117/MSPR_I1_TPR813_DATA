"""
Trouve l'affiliation politique d'une personne donnée
"""
import html
import logging
import os
import re
import time
from collections import Counter
from typing import List, Dict

import pandas as pd
import requests
from requests import Session

from src.political import PROJECT_DIR, __logger, conn
from src.political.queries import check_table, get_parties_politiques, get_resultats_elections
from src.utils import parallelize


def extract_orientation(line):
    """
    Retourne Gauche/Centre/Droite en fonction des résultats trouvés dans la ligne actuelle
    """
    line = line.lower()
    orientation = None
    if 'centre' in line:
        orientation = 'centre'
    elif 'gauche' in line:
        orientation = 'gauche'
    elif 'droite' in line:
        orientation = 'droite'
    return orientation


class OrientationFinder:
    """
    Charge la liste des orientations politiques de chaque parti à l'initialisation
    à partir de Wikipédia
    quand on demande l'orientation politique d'un parti, on cherche dans la liste
    """

    def __init__(self):
        self.__logger = logging.getLogger(self.__class__.__name__)
        self.__logger.info("Loading parties orientations...")
        self.__parties = {}
        __start_time = time.perf_counter()
        self.__session = None
        self.__load_parties()
        self.__logger.info("Done loading in %s seconds", time.perf_counter() - __start_time)
        self.__logger.debug("Loaded : %s" % self.__parties)

    def find_orientation(self, party_names):
        """
        Trouve l'orientation politique d'un parti donné
        s'arrête au premier parti trouvé dans la liste
        :param party_names: noms des partis politiques à vérifier
        :return: orientation politique des partis donnés
        """
        if not party_names:
            return None
        self.__logger.debug("Finding orientation of %s", party_names)
        if isinstance(party_names, str):
            party_names = [party_names]
        for party_name in party_names:
            party_name = party_name.lower()
            # remove accents and special characters
            party_name = re.sub(r'[éèêë]', 'e', party_name)
            party_name = re.sub(r'[àâä]', 'a', party_name).upper()
            if '/' in party_name:
                orientation = self.find_orientation(set(party_name.split('/')))
                if orientation:
                    return orientation
            try:
                return self.__parties[party_name]
            except KeyError:
                self.__logger.warning("Party %s not found in parties list", party_name)
        return None

    def __load_parties(self):
        """
        Charge la liste des partis politiques et leur orientation politique en base de données
        """
        if check_table('parties') is False:
            self.__generate_initial_dataset()
            return
        for _, row in get_parties_politiques().iterrows():
            self.__parties[row['name']] = row['orientation']
        return

    def __set_party_orientation(self, political_houses_acronyms_on_line, orientation):
        for political_house_acronym in political_houses_acronyms_on_line:
            political_house_acronym = political_house_acronym[1:-1]
            self.__parties[political_house_acronym] = orientation

    def __generate_initial_dataset(self):
        """
        En l'absence du dataset // lignes en DB, on récupère les informations pour avoir des données à chercher.
        """
        if self.__session is None:
            self.__session = requests.Session()
        wiki_page = get_wiki_page("Liste_de_partis_politiques_en_France", section=5, session=self.__session)
        if not wiki_page:
            raise Exception("Error while fetching wiki page")
        for key, value in wiki_page:
            if key != '*':
                continue
            orientation = None
            for line in value.split('\n'):
                line = line.strip()
                if line.startswith('='):
                    continue
                if line.startswith('{'):  # always comes before the party name
                    orientation = extract_orientation(line)
                else:
                    acronyms = re.findall(r'(?:\()[A-Z]+(?:\))', line)
                    if acronyms:
                        self.__set_party_orientation(acronyms, orientation)
        df = pd.DataFrame([{'name': k, 'orientation': v} for k, v in self.__parties.items()])
        df.to_sql('parties', conn, if_exists="replace")

    def find_one(self, __name):
        """
        Trouve l'orientation politique de la personne donnée
        """
        start = time.perf_counter()
        result = {'name': __name, 'orientation': get_orientation_politique(__name, self)}
        logging.info("Found %s in %s seconds", result, time.perf_counter() - start)
        return result

    def find_all(self, __names):
        """
        Trouve l'orientation politique de chaque personne dans la liste
        Une personne nommée "AB CD E" match "AB CD" et "CD E"
        """
        start = time.perf_counter()
        result = parallelize(self.find_one, __names)
        self.__logger.info(f"Found {len(result)} in %s seconds", time.perf_counter() - start)
        uq_names = []
        for r in result:
            name = r['name']
            for uq in uq_names:
                if name in uq or uq in name:
                    r['name'] = uq
                    break
            if name not in uq_names:
                uq_names.append(r['name'])

        return result


def get_wiki_page(page_name, section: int = None, method='wikitext', session: Session = None, retries=3):
    """
    Charge la page Wikipédia demandée
    :param page_name: la page Wikipédia à charger
    :param section: (option) la section de la page à charger
    :param method: (option) la méthode de parsing de la page
                   [default : 'wikitext']
    :param session: (option) la session requests à utiliser
    :param retries: (option) le nombre de tentatives de chargement de la page
    :return: dict with keys '*' and 'alerts'
    """
    extra = ''
    __json = None
    if retries <= 0:
        return __json

    if section:
        extra = f"&section={section}"
    __logger.info("Searching %s", page_name)
    url = (f"https://fr.wikipedia.org/w/api.php?"
           f"action=parse"
           f"&prop={method}"
           f"&redirects=1"
           f"&format=json"
           f"&page={html.escape(page_name)}"
           f"{extra}")
    if session is None:
        session = requests.Session()
    try:
        response = session.get(url, allow_redirects=True)
        if response.status_code != 200:
            raise Exception(f"Error {response.status_code} while fetching {url}")
        if 'error' in response.json():
            return None
        __json = response.json()['parse'][method]
        return __json.items()
    except Exception as e:
        print(e)
        return get_wiki_page(page_name, section, method, session, retries - 1)


def __get_valuable_lines(parsed_wiki_page):
    """
    Trouve
        | parti = [[<NomParti>|<ACRO>]] <small>(<start_date>-<end_date>)</small><br>
                  [[<parti>|<...>]] <small>(depuis <start_date>)</small>
    :param parsed_wiki_page:
    :return: any affiliation lines found
    """
    if parsed_wiki_page is None:
        raise ValueError("parsed_wiki_page must be given")
    found = False
    for key, value in parsed_wiki_page:
        if key == '*':
            for line in value.split('\n'):
                line = line.strip()
                if line.startswith('='):
                    continue
                if line.startswith('|') and ' parti ' in line:
                    yield line
                    found = True
    if not found:
        logging.warning("No affiliation found in %s", parsed_wiki_page)
    return


def extract_affiliation(line) -> List[str]:
    """
    Extract affiliations politiques de la ligne
    exemple: [[Parti socialiste (France)|PS]] <small>(2006-2009)</small><br>[[Renaissance (parti)|EM/LREM/RE]] <small>(depuis 2016)</small>
    :param line: line to extract affiliation from
    :return: any potential affiliation found
    """
    line = html.unescape(line)
    political_engagements = []
    for affiliation in re.findall(r'\[\[(.*?)]]', line):
        if '|' in affiliation:
            affiliation = affiliation.split('|')[1]
            if affiliation.startswith('{'):
                continue
            political_engagements.append(affiliation)

    return political_engagements


def get_orientation_politique(person_name, of: OrientationFinder, swapped=False):
    """
    Trouve l'affiliation politique d'une personne donnée
    :param person_name: nom de la personne
    :param of: classe OrientationFinder
    :param swapped: (option) si True, n'applique pas la transformation NOM Prenom -> Prenom NOM,
                    si la page initiale n'est pas trouvée
    :return: dict with keys 'name', 'affiliations' and 'orientation'
    """
    if not person_name:
        raise ValueError("person_name must be given")
    if not isinstance(person_name, str):
        raise ValueError("person_name must be a string")
    if not of:
        raise ValueError("orientation_finder must be given (class OrientationFinder)")
    page_name = re.sub("[_-]", ' ', person_name)
    page_name = '_'.join([s.capitalize() for s in page_name.split(' ') if len(s) > 1])
    if not swapped:
        split: List[str] = person_name.split(' ')
        if len(split) >= 3:  # si le (compose) NOM Prenom-Compose
            return get_orientation_politique(f"{split[2]} {split[0]} {split[1]}", of, swapped=True)
        elif len(split) == 2:  # si le NOM Prenom-Compose
            return get_orientation_politique(" ".join(reversed(split)), of, swapped=True)
    __dict = get_wiki_page(page_name)
    if __dict is None:
        logging.warning("No wiki page found for %s", page_name)
        return None

    _valuable = __get_valuable_lines(__dict)
    orientation_list = []
    for line in _valuable:
        if line.startswith('|'):
            orientation = of.find_orientation(extract_affiliation(line))
            orientation_list.append(orientation)
    c = Counter(orientation_list).most_common(1)[0][0] if orientation_list else None
    logging.debug("Found %s for %s", c, person_name)
    return c


def find_orientation_politique(__names: List[str]) -> List[Dict]:
    """
    Trouve l'affiliation politique de chaque personne
    :param __names: liste des noms des personnes
    :return: json des orientations politiques des personnes trouvées (value=None si non trouvée)
    """
    orientation_finder = OrientationFinder()
    return orientation_finder.find_all(__names)


def save_orientation_politique(__jsons, file_name):
    df = pd.DataFrame(__jsons, columns=['name', 'orientation'])
    destination = os.path.join(PROJECT_DIR, 'data', 'clean')
    df.to_csv(os.path.join(destination, f"{file_name}.csv"), index=False, sep=';')
    df.to_sql(f"{file_name}", conn,
              if_exists="replace")
    __logger.info("Saved %s", destination)


if __name__ == '__main__':
    start_time = time.perf_counter()
    dataset = get_resultats_elections()
    logging.info(f"Loaded presidential dataset in {time.perf_counter() - start_time} seconds")
    names = dataset['CANDIDAT'].unique()
    affiliations_dict = find_orientation_politique(names)
    print(affiliations_dict)
    save_orientation_politique(affiliations_dict, 'orientations_politiques')
