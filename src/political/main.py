import os
import time

import pandas as pd

from src.political import PROJECT_DIR
from src.political.afiliation_politique import find_orientation_politique, save_orientation_politique
from src.political.queries import get_resultats_elections
from src.political.transform_resultats_elections import flatten_candidats, to_db, get_document

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Transforms the given url's document to a csv file."
                                                 "--url : (required) the url to fetch the document from."
                                                 "--save_name : (required) the name to save the document to."
                                                 "--help : display this help message.")
    parser.add_argument("--save_extract", type=str, required=False,
                        default="resultats_elections_cumul_presidentielles_rennes.csv")
    parser.add_argument("--save_name", type=str, required=False, default="resultats_elections_rennes_metropole_flatten")
    parser.add_argument("--url", type=str, required=False,
                        default="https://data.rennesmetropole.fr/api/explore/v2.1/catalog/datasets/"
                                "resultats_elections_cumul_presidentielles_rennes/exports/csv"
                                "?lang=fr"
                                "&timezone=Europe%2FBerlin"
                                "&use_labels=true"
                                "&delimiter=%3B")
    args = parser.parse_args()

    get_document(args.url, args.save_extract)
    print("Document saved. Transforming : " + args.save_extract)
    __file_to_transform = args.save_extract.split('.')[0]
    df = pd.read_csv(os.path.join(PROJECT_DIR, 'data', 'extracted', f"{__file_to_transform}.csv"), sep=';')
    print(df.columns)
    # remove any columns that start with "geo"
    df = df[[col for col in df.columns if not col.startswith("geo") and not col.startswith("Geo")]]

    flatten = flatten_candidats(args.save_extract)
    print("Flattened document. Saving to : " + args.save_name)
    to_db(flatten, args.save_name)

    start_time = time.perf_counter()
    dataset = get_resultats_elections()
    names = dataset['CANDIDAT'].unique()
    affiliations_dict = find_orientation_politique(names)
    save_orientation_politique(affiliations_dict, 'orientations_politiques')
