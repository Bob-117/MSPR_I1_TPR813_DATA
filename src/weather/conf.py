import logging
import os
import sqlite3

import colorama

SRC_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.dirname(SRC_DIR).split('src')[0]
DATA_DIR = os.path.join(ROOT_DIR, 'data')

colorama.init()
# Connect to the database
table_name = "weather"
conn = sqlite3.connect(os.path.join(DATA_DIR, f'{table_name}.db'))
logging.basicConfig(level=logging.INFO)
__logger = logging.getLogger(__file__.replace('\\', '/').split('/')[-1].split('.')[0])


table_name = "weather"
weather_columns = ["temperature_2m", "relative_humidity_2m", "dew_point_2m", "apparent_temperature", "precipitation",
                   "rain", "pressure_msl", "surface_pressure", "evapotranspiration", "et0_fao_evapotranspiration",
                   "vapour_pressure_deficit", "wind_speed_10m", "wind_speed_80m", "wind_speed_120m",
                   "wind_direction_80m", "wind_direction_120m", "wind_gusts_10m", "temperature_80m", "temperature_120m",
                   "soil_temperature_0cm", "soil_temperature_6cm", "soil_temperature_18cm", "soil_temperature_54cm",
                   "soil_moisture_0_to_1cm", "soil_moisture_1_to_3cm", "soil_moisture_3_to_9cm",
                   "soil_moisture_9_to_27cm", "soil_moisture_27_to_81cm"]

# Create the table if it doesn't exist
conn.execute(f'''
CREATE TABLE IF NOT EXISTS {table_name} (
    time TEXT PRIMARY KEY,
    {', '.join([f'{col} REAL' for col in weather_columns])}
);
''')

def project_ressource(file, dir='data', create=False):
    __path = os.path.join(ROOT_DIR, dir, file)
    if os.path.exists(__path):
        return __path
    elif create:
        # touch
        if not os.path.exists(os.path.dirname(__path)):
            os.makedirs(os.path.dirname(__path))
        with open(__path, 'w') as f:
            f.write('')
            f.close()
        return __path
    raise FileNotFoundError(f"File {file} not found in {dir}")
