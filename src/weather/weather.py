import datetime
from typing import Tuple, List

import pandas as pd
import pyspark
import seaborn as sns
from matplotlib import pyplot as plt
from pyspark.ml.clustering import KMeans
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression

import conf
from conf import conn, project_ressource
from src.utils import get_spark_session
from src.weather.openweather import fetch_data

tasks = set()


def create_spark_df_using_pandas(data, session) -> pyspark.sql.DataFrame:
    # Create a dataframe from the data
    pandas_df = pd.DataFrame(data)
    pandas_df = clean_df(pandas_df)

    df = session.createDataFrame(pandas_df)
    return df


def clean_df(df) -> pd.DataFrame:
    time_val = df['time'][0]
    time_fmt = '%Y-%m-%dT%H:%M' if 'T' in time_val else '%Y-%m-%d %H:%M:%S'
    df['time'] = pd.to_datetime(df['time'], format=time_fmt)
    # place time as index
    df.set_index(df['time'], inplace=True)

    # Clean the dataframe
    df['relative_humidity_2m'] = df['relative_humidity_2m'].astype(float) / 100
    return df


def check_last_update() -> datetime.datetime:
    # get first line from db with a null value
    try:
        cursor = pd.read_sql('SELECT * FROM updates', conn)
    except pd.io.sql.DatabaseError:
        # create the table
        pd.DataFrame({'time': [datetime.datetime.now()]}).to_sql('updates', if_exists='replace', index=False, con=conn)
        cursor = pd.read_sql('SELECT * FROM updates', conn)

    if cursor.empty:
        return None
    return pd.to_datetime(cursor.iloc[0]['time'])


def main_load(session) -> Tuple[pyspark.sql.DataFrame, pyspark.sql.Column]:
    # last_update = check_last_update()
    # if last_update is None or (datetime.datetime.now() - last_update).days >= 1:
    data = fetch_data(from_time=None)
    df_spark = create_spark_df_using_pandas(data['hourly'], session)

    # save to sql database to avoid reprocessing the data from the api
    pd_df = df_spark.toPandas()
    pd_df.to_sql('weather', if_exists='replace', index=False, con=conn)

    df_units = pd.DataFrame(data['hourly_units'], index=[0])
    df_units.to_sql('units', if_exists='replace', index=False, con=conn)

    df_now = pd.DataFrame({'time': [datetime.datetime.now()]})
    df_now.to_sql('updates', if_exists='replace', index=False, con=conn)

    # split facts and forecast data to prepare training and testing
    facts = df_spark.filter(df_spark.time < datetime.datetime.now())
    facts = facts.toPandas()
    facts.to_sql('api_facts', if_exists='replace', index=False, con=conn)

    forecast = df_spark.filter(df_spark.time >= datetime.datetime.now())
    forecasts = forecast.toPandas()
    forecasts.to_sql('api_forecasts', if_exists='replace', index=False, con=conn)

    # save facts & forecast data to a table

    # else:
    #     df_spark = create_spark_df_using_pandas(pd.read_sql('SELECT * FROM weather', conn))
    #     df_units = pd.read_sql('SELECT * FROM units', conn)

    return df_spark, df_units.iloc[0]


def save_csv(_csv, path):
    with open(path, 'w') as f:
        f.write(_csv)
        f.close()


def insits(df_spark: pyspark.sql.DataFrame):
    """
    Display the first 5 rows of the dataframe, describe the dataframe and show the schema
    """
    # standard operations
    df_spark.show()
    df_spark.printSchema()
    df_spark.describe().show()


def training_insits(df_spark: pyspark.sql.DataFrame, targets: List[str]):
    """
    plot as many insights as possible to understand the data
    """
    # basic plotting for insits on data to train model
    pd_df = df_spark.toPandas()
    cols = list(pd_df.columns)

    # corelation matrix
    corr = pd_df.corr()
    # plot blue-red heatmap
    fig = plt.matshow(corr, cmap='coolwarm')
    plt.colorbar(fig)
    plt.xticks(range(len(corr.columns)), corr.columns, rotation=90)
    plt.yticks(range(len(corr.columns)), corr.columns)

    plt.show()

    for target in targets:
        print(f"For {target} :\n{corr[target].sort_values(ascending=False)}")
        for _target in targets:
            if _target == target:
                continue
            print(f"Correlation with {target} :\n{corr[_target].sort_values(ascending=False)}")


def lin_reg(train_data, targets):
    model = LinearRegression(featuresCol='features', labelCol='temperature_2m', predictionCol='prediction')
    model = model.fit(train_data)
    print("Coeficients : ", model.coefficients)
    print("Interceptors : ", model.intercept)
    print("Summary :\n")
    print("Predictions :\n", model.summary.predictions.show())
    print("R2 : ", model.summary.r2)
    print("RMSE : ", model.summary.rootMeanSquaredError)
    print("MAE : ", model.summary.meanAbsoluteError)
    print("MSE : ", model.summary.meanSquaredError)
    print("Explained variance : ", model.summary.explainedVariance)
    print("Residuals : ", model.summary.residuals.show())
    fg = model.summary.residuals.toPandas().plot(kind='hist', bins=50, figsize=(12, 6))
    fg.set_title("Residuals")
    fg.set_ylabel("Frequency")
    plt.savefig(project_ressource("plots/residuals.png", create=True))
    print("P-Values : ", model.summary.pValues)
    print("T-Values : ", model.summary.tValues)
    print("Objective history : ", model.summary.objectiveHistory)
    print("Deviance residuals : ", model.summary.devianceResiduals)
    fg.set_title("Deviance residuals")
    plt.savefig(project_ressource("plots/deviance_residuals.png", create=True))
    print("Num of iterations : ", model.summary.totalIterations)
    print("Degrees of freedom : ", model.summary.degreesOfFreedom)
    return model


def eval_kmeans(model, train_data, predcictions, k):
    # color points by cluster
    print("Cluster centers : ", model.clusterCenters())
    print("Predictions : ", predcictions.show())
    print("K : ", k)
    print("Silhouette : ", model.summary.clusterSizes)
    print("Cost : ", model.summary.trainingCost)
    print("Summary : ", model.summary.predictionCol)

    fg = sns.scatterplot(data=predcictions, x='apparent_temperature', y='prediction', palette='viridis',
                         hue='prediction')
    # append centroids with red crosses
    centroids = model.clusterCenters()
    ## Cluster centers :  [array([8.68843323e-03, 6.16409043e+00, 6.47213460e-02, 5.82544690e-02,
    # 1.01652923e+03, 1.00015205e+03, 3.06309148e-02, 3.60304942e-02,
    # 1.59353312e-01, 1.76130915e+01, 2.76650894e+01, 3.13934805e+01,
    # 2.39252366e+02, 2.41409569e+02, 2.99003155e+01, 8.20825447e+00,
    # 7.99421661e+00, 8.06424816e+00, 8.03028391e+00, 7.93501577e+00,
    # 8.09921136e+00, 3.13882755e-01, 3.14423764e-01, 3.15665089e-01,
    # 3.18423764e-01, 3.21654574e-01]), array([8.71129363e-03, 5.40041068e-02, 8.50102669e-02, 7.49486653e-02,
    # 1.02031006e+03, 1.00350103e+03, 2.20739220e-02, 2.63449692e-02,
    # 1.08850103e-01, 1.24815195e+01, 1.94414784e+01, 2.19872690e+01,
    # 7.38316222e+01, 7.76119097e+01, 2.09765914e+01, 2.27433265e+00,
    # 2.24517454e+00, 2.53182752e+00, 3.31293634e+00, 4.50513347e+00,
    # 6.66221766e+00, 3.13266940e-01, 3.14211499e-01, 3.15546201e-01,
    # 3.18963039e-01, 3.23425051e-01]), ...]
    for i, centroid in enumerate(centroids):
        fg.scatter(centroids[1], centroid[0], c='red', marker='x', s=100)
    # set the title and labels
    fg.set_title(f"KMeans with {k} clusters")
    fg.set_xlabel("Temperature 2m")
    fg.set_ylabel("Apparent temperature")

    # save and show
    plt.savefig(project_ressource(f"plots/kmeans_{k}.svg", create=True))
    plt.show()

    # eval
    print("Silhouette with squared euclidean distance : ", model.summary.trainingCost)


def kmeans(train_data, targets):
    best_model = None
    print("Training KMeans...")
    fig1 = sns.violinplot(df, x=targets[0], y=targets[1], palette='coolwarm')
    fig1.set_title(f'Violin plot of {targets[1]} by {targets[0]}')
    plt.savefig(project_ressource("plots/violin_plot.svg", create=True))
    plt.show()
    for k in range(len(list(train_data.columns)) // 2,
                   len(list(train_data.columns)),
                   len(list(train_data.columns)) // 10 + 1):
        model = KMeans(featuresCol='features', k=k)
        model = model.fit(train_data)
        predict = model.transform(train_data)
        # eval_kmeans(model, train_data, predict, k)
        if best_model is None or model.summary.trainingCost < best_model.summary.trainingCost:
            best_model = model
    return best_model


if __name__ == '__main__':
    # f = keep_alive()
    # f.cancel()
    spark_session = get_spark_session('Weather')
    print(spark_session)

    df_spark, units = main_load()
    print(df_spark, "\n" * 3, units)
    print(df_spark.persist().count())

    # basic visualisation
    insits(df_spark)

    # features selection
    all_cols = conf.weather_columns
    targets = ['temperature_2m', 'apparent_temperature']
    features = [col for col in all_cols if col not in targets]

    # advanced insigths for training, before continuing
    training_insits(df_spark, targets)

    df = df_spark.toPandas()
    _csv_file = df.to_csv(project_ressource("weather.csv"), sep=",", index=False, header=True)

    # save to sql database to avoid reprocessing the data from the api
    # df.to_sql('weather', if_exists='replace', index=False, con=conn)

    # features selection

    # assemble to vectors
    assembler = VectorAssembler(inputCols=features, outputCol='features', handleInvalid="skip")
    print("#" * 20, "ASSEMBLER", "#" * 20, assembler)

    train_data = assembler.transform(df_spark)
    print(train_data.show())
    print(train_data.printSchema())
    print(train_data.describe().show())
    print(train_data.persist().count())

    ################################## TRAINING ##################################
    # train the model
    model = kmeans(train_data, targets)
    print(model.summary.predictionCol)
    test_data = pd.read_sql('SELECT * FROM weather_forecasts', conn)
    pred = model.transform(test_data)
    eval_kmeans(model, train_data, pred, model.getK)
    # model2 = lin_reg(train_data, targets)
    input("Press Enter to exit...")
