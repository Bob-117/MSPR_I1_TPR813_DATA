import datetime
import json
import logging

import requests

from src.utils import save_json
from src.weather.conf import project_ressource, weather_columns

__base_url = "https://api.open-meteo.com/v1/forecast?"

location = {
    'latitude': 48.1173,
    'longitude': 1.6778,
}
time_ = {
    'hourly_units': 'metric',
    'hourly': ','.join(weather_columns),
    "past_days": 5,  # max 92
    "forecast_days": 16  # max 16
}


def call_weather_api(from_time=None) -> json.JSONDecoder:
    # Get the data from the API
    __weather_api_params = location.copy()
    __weather_api_params.update(time_)
    if from_time:
        print(from_time, datetime.datetime.now() - from_time)
        __weather_api_params.update({'from': from_time})
    url = __base_url + f"{'&'.join([f'{k}={v}' for k, v in __weather_api_params.items()])}"
    logging.info(f"Contacting {url}")
    response = requests.get(url)
    data = response.json()
    # Save the data to a file
    return data


def fetch_data(from_time, save=False) -> json.JSONDecoder:
    # Get the data from the API
    data = call_weather_api(from_time)
    # Save the data to a file
    if save:
        _target = project_ressource("data.json", create=True)
        save_json(data, _target)
    return data
