import pandas as pd
import os
from .data_transform import print_dataset

FEATURES = ['Code.département', 'classe', 'annee', 'faits']
COL_NAMES = {
    'Code.département': 'city',
    'classe': 'event',
    'annee': 'year',
    'faits': 'nb'
}


def create_insecurity_dataset(
        _file_in, _file_out='infractions_departement_annee.csv', _dir='data', features=None, save=False
):
    if features is None:
        features = FEATURES
    df = pd.read_csv(f'{_dir}/{_file_in}', sep=';')
    features_df = df[features]
    features_df = features_df.rename(columns=COL_NAMES)
    if save:
        location = os.path.join(_dir, _file_out)
        print(f'Saving new dataset with features {features} at {location}')
        features_df.to_csv(location, index=False)
        # print('move the file in data')
    return features_df


def total_event_by_type_by_year(_d, _filter=False):
    """
    ALl
    :param _d:
    :param _filter:
    :return:
    """
    if _filter:
        _d = _d[_d['event'].isin([
            'Cambriolages de logement',
            'Coups et blessures volontaires',
            'Destructions et dégradations volontaires',
            'Trafic de stupéfiants'
        ])]
    event_sum_per_year = _d.groupby(['year', 'event'])['nb'].sum().reset_index()
    event_sum_per_year_pivot = event_sum_per_year.pivot(index='year', columns='event', values='nb').reset_index()
    # event_sum_per_year_pivot = event_sum_per_year_pivot.fillna(0)
    # a = event_sum_per_year_pivot
    # print('============================================================================')
    # print(len(a))
    # print(a.columns)
    # print('============================================================================')
    return event_sum_per_year_pivot


def total_event_by_city_by_year(_d):
    total_events_per_city_year = _d.groupby(['city', 'year'])['nb'].sum().reset_index()
    return total_events_per_city_year


def total_events_by_year(_d):
    _total_events_by_year = _d.groupby('year')['nb'].sum().reset_index()
    _total_events_by_year = _total_events_by_year.rename(columns={'nb': 'total_events'})
    return _total_events_by_year


def insecurity_stats(_d, name='all_cities', save=True):
    total_events_by_year_d = total_events_by_year(_d)
    print_dataset(total_events_by_year_d, title=f'total_events_by_year : {name} ')
    if save:
        total_events_by_year_d.to_csv(f'data/clean/insecurity/tot_events_by_y_{name}.csv', index=False)

    # print_dataset(total_event_by_city_by_year(_d), title=f'total_event_by_city_by_year : {name} ')
    total_event_by_city_by_year_d = total_event_by_city_by_year(_d)
    print_dataset(total_event_by_city_by_year_d, title=f'total_event_by_city_by_year : {name} ')
    if save:
        total_event_by_city_by_year_d.to_csv(f'data/clean/insecurity/total_event_by_city_by_y_{name}.csv', index=False)

    # print_dataset(total_event_by_type_by_year(_d), title=f'total_event_by_type_by_year : {name} ')
    total_event_by_type_by_year_d = total_event_by_type_by_year(_d)
    print_dataset(total_event_by_type_by_year_d, title=f'total_event_by_type_by_year : {name} ')
    if save:
        total_event_by_type_by_year_d.to_csv(f'data/clean/insecurity/total_event_by_type_by_y_{name}.csv', index=False)


def create_data_for_one_city(_d, city):
    city_d = _d[(_d['city'] == city)]
    return city_d


def handle_one_city(_d, city, save=False):
    city_d = create_data_for_one_city(_d, city)
    print_dataset(city_d, title='Rennes 35')
    insecurity_stats(city_d, name='Rennes 35')
    print_dataset(city_d)
    # for year_index in city_d['year'].unique():
    for year_index in range(len(city_d['year'].unique())):
        for event in city_d['event'].unique():
            try:
                value = total_event_by_type_by_year(city_d).at[year_index, event]
                y = total_event_by_type_by_year(city_d)['year'][year_index]
                print(f'{value} {event} in year 20{y}')
                # # get total at rennes => %
                # # get rennes / all cities %
            except Exception as e:
                print(f'year index : {year_index}')
                print(f'event : {event}')