import json
import logging
from multiprocessing import Pool

import pandas as pd
import requests
from pandas.io.sas.sas_constants import magic
from pyspark.sql.session import SparkSession

from src.data_extract import find_file


def parallelize(fn, *args):
    with Pool() as executor:
        __out = executor.map(fn, *args)
    return __out


def save_json(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)
        f.close()

def get_content(url):
    with requests.get(url) as response:
        response.raise_for_status()
        return response.content


def save_content_to_file(content, file_path):
    with open(file_path, 'wb') as file:
        file.write(content)


def get_spark_session(app_name):
    sc = (SparkSession.builder
          .appName(app_name)
          .master('local[*]')
          .config('spark.driver.memory', '1g')
          .config('spark.executor.memory', '1g')
          .getOrCreate())
    sc.sparkContext.setLogLevel("ERROR")
    logging.info(f"""Spark session created : {sc}
    App name : {app_name}
    Master : {sc.sparkContext.master}
    Driver memory : {sc.sparkContext.getConf().get('spark.driver.memory')}
    Executor memory : {sc.sparkContext.getConf().get('spark.executor.memory')}
    Spark Version : {sc.version}
    Scala Version : {sc.sparkContext._jvm.scala.util.Properties.versionString()}
    Java Version : {sc.sparkContext._jvm.java.lang.System.getProperty("java.version")}
    Hadoop Version : {sc.sparkContext._jvm.org.apache.hadoop.util.VersionInfo.getVersion()}
    
    """)
    return sc


def load_file(file_name):
    df = file_to_dataframe(file_name)
    # if f.endswith('.xlsx'):
    #     df = pd.read_excel(f)
    # elif f.endswith('.csv'):
    #     df = pd.read_csv(f)
    # else:
    #     raise NotImplemented('Cant load this type of file')
    # return df


def file_to_dataframe(file_name):
    f = find_file(file_name=file_name)
    t = file_type(f)
    if 'openxml' in t:
        df = pd.read_excel(f)
    elif 'csv' in t:
        df = pd.read_csv(f)
    else:
        raise NotImplemented('Cant load this type of file')
    return df


def file_type(file_name):
    mime = magic.Magic(mime=True)
    t = mime.from_file(file_name)
    print(f'Type found for file {file_name} : {t}')
    return t

#
# if __name__ == '__main__':
#     root_directory = '.'
#     data_directory = __find_file_location(root_directory, 'data')
#     print(f"Data directory found at: {data_directory}")
