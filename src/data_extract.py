import os
import logging

logging.basicConfig(level=logging.DEBUG)

########################################################################################################################
# VARIABLES
########################################################################################################################
# edit this value (where are ur data src files)
BASE_PROJECT = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
BASE_DATA_PATH = os.path.join(BASE_PROJECT, 'data')
BASE_DATA_FILE_PATH = os.path.join(BASE_PROJECT, BASE_DATA_PATH)


########################################################################################################################
# CSV FILE & DATASET
########################################################################################################################
def __find_file_location(target_dir, root_dir=BASE_DATA_FILE_PATH):
    """
    Find a file location in the project DATA directory
    :param target_dir:
    :param root_dir:
    :return:
    """
    location = os.path.join(root_dir, target_dir)
    if os.path.exists(location) and os.path.isdir(location):
        logging.info(f'Found location {location}')
        return location
    raise FileNotFoundError(f'=====> Could not find {target_dir} in {root_dir}.')


def __find_file(location, file_name):
    file_path = os.path.join(location, file_name)
    if os.path.exists(file_path) and os.path.isfile(file_path):
        logging.info(f'Found file path : {file_path}')
        return file_path
    raise FileNotFoundError(f'=====> Could not find {file_name} in {location}.')


def find_file(file_name, target_dir=BASE_DATA_FILE_PATH):
    try:
        location = __find_file_location(target_dir)
    except FileNotFoundError:
        logging.error(f'=====> Cant find the data directory {target_dir}')
        raise
    try:
        file = __find_file(location=location, file_name=file_name)
    except FileNotFoundError:
        logging.error(f'=====> Cant find file {file_name} in {target_dir}')
        return None
    logging.info(f'File {file_name} found')
    return file


def process():
    FILES = [
        (os.path.join('raw', 'csv'), 'donnee-dep-data.gouv-2022-geographie2023-produit-le2023-07-17.csv'),
        ('clean', 'rennes.csv'),
        (os.path.join('raw', 'xlsx'), 'resultats-par-niveau-burvot-t1-france-entiere.xlsx'),
        ('', 'data.csv'),
        ('', 'data1.csv'),
        (os.path.join('raw', 'xlsxx'), 'data2.csv'),
        ('raw', 'data3.csv'),
    ]

    for file_info in FILES:
        _dir, _file = file_info[0], file_info[1]
        print(f'===== {_file} =====')
        try:
            if _dir:
                f = find_file(file_name=_file, target_dir=_dir)
            else:
                f = find_file(file_name=_file)
            print(f)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    process()
