# MSPR

## Contexte

```
La manipulation d'opinion et la lutte informatique d'influence ne sont en aucun cas le but de ce projet.
```

### Sujet

```
Jean-Edouard de la Motte Rouge a créé une start-up spécialisée dans le conseil sur la thématique des
campagnes électorales.
La start-up comprend un.e expert.e en analyse politique, un.e business developper, et un.e assistant.e.
Il souhaite pouvoir prédire, grâce à l’intelligence artificielle, les tendances des élections à venir, en se
basant sur un certain nombre d’indicateurs, comme la sécurité, l’emploi, la vie associative, la population,
la vie économique (nombre d’entreprises), la pauvreté…
Cela lui donnerait un avantage concurrentiel important dans son activité.
Avant d’investir dans une infrastructure et une politique de recherche et développement en ce sens, et
dans l’idée de demander des aides à l’innovation, il fait appel à votre groupe pour établir une preuve de
concept (POC).
La POC devra être conçue pour un secteur géographique restreint (ville, arrondissement, circonscription,
département…) et unique.
Afin de répondre à sa commande, il conviendra donc de :
1. Sélectionner un secteur géographique sur lequel sera établie la POC
2. Sélectionner différents jeux de données parmi ceux disponibles ci-dessous et/ou de votre choix, afin
de rechercher des corrélations entre résultats des élections passées et contexte
3. Etablir un petit nombre de visualisations graphiques montrant les jeux de données choisis pour leur
donner de la lisibilité
4. Etablir un modèle prédictif supervisé, en découpant les données à disposition en un ou plusieurs jeux
d’entraînement et jeux de tests afin de vérifier la fiabilité de votre modèle
5.Proposer une visualisation graphique de votre modèle et des prédictions à 1 an, 2 ans et 3 ans.
```

- Réaliser un POC d’un outil de prévision électorale avec du machine learning
- Utilisateurs : un.e expert.e en analyse politique, un.e business developper, et un.e assistant.e
- Indicateurs : la sécurité, l’emploi, la vie associative, la population, la vie économique (nombre d’entreprises), la pauvreté
- Périmètre : La ville de Rennes

### Grille d'évaluation

|   | Compétences                                                                                                                                                                                                                                                   | Critères d'évaluation                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                                                                                                                    |
|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| 1 | Collecter les besoins en données des directions métiers de l’entreprise afin d’avoir une vision structurée de l’ensemble des données du système d’information et partager la stratégie Data globale avec le comité de direction                               | Le candidat/La candidate propose une stratégie de gestion de données globale :<br>-Il/Elle collecte les besoins en termes de données des directions métiers à partir du cahier des charges<br>-Il/Elle modélise le processus à déployer pour collecter, structurer, gérer et valoriser les données<br>-Il/Elle formalise un descriptif écrit de son processus et liste ses sources de données (vocabulaire technique adapté, timing du processus adapté à la réalisation)<br>-Il/Elle illustre son écrit par un schéma (il/elle présente un diagramme de flux à l’aide d’un outil approprié type BPM ou ETL) | Diagramme de flux non demandé ?                                                                                    |
| 2 | Définir une architecture business intelligence à partir des orientations stratégiques arrêtées avec le comité de direction afin de mettre à disposition des utilisateurs métiers les données structurées d’un S.I.                                            | Le candidat/La candidate est capable de proposer une architecture décisionnelle en décrivant :<br>-L’environnement technologique de la couche 1 (collecte de données)<br>-L’environnement technologique de la couche 2 (la modélisation et le stockage de données)<br>-Il/Elle présente les différents outils utilisés pour la couche 3 (restitution de données)                                                                                                                                                                                                                                             |                                                                                                                    |
| 3 | Définir une stratégie big data (de la collecte aux traitements des données) à partir des orientations stratégiques arrêtées avec le comité de direction afin d’aider l’entreprise à mieux comprendre ses clients et à créer de nouveaux services              | Le candidat/La candidate définit une stratégie big data pour répondre aux besoins des clients ou métiers de l’entreprise :<br>-Il/Elle liste les technologies et les outils adaptés aux types de données (couche ingestion de données)<br>-Il/Elle choisit une méthodologie de stockage de type ELT ou Datalake<br>-Il/Elle modélise les processus de traitement parallèle et distribué (pipelines)<br>-Il/Elle définit le mode de restitution des données afin de créer de la valeur                                                                                                                        | Pipelines jamais demandée, on extrait les données d'un .CSV ?                                                      |
| 4 | Proposer des modèles statistiques et de data science (machine Learning) à mettre en pratique aux directions métiers afin de détecter des nouveaux services, anticiper des besoins et résoudre des problématiques métiers de l’entreprise                      | A partir d’une approche machine learning existante, le candidat/la candidate est capable d’implémenter et tester des modèles de machine learning (data science) : <br>-Il/Elle utilise de façon approprié le langage R ou Python<br>-Il/Elle interprète les résultats en les formalisant à l’écrit<br>-Le modèle conçu par le candidat/la candidate atteint un pouvoir de prédiction supérieur à 0.5                                                                                                                                                                                                         | Pouvoir de prédiction supérieur à 0.5 ? mais si les recherches ne mènent à rien ? si le modèle n'y arrive jamais ? |
| 5 | Organiser les sources de données sous forme de résultats exploitables (data visualisation) pour alimenter les outils décisionnels et visualiser les résultats de façon compréhensible permettant d’aider les directions métiers à la prise de décision        | Le candidat/La candidate est capable de valoriser graphiquement des résultats issus des données collectées :<br>-Capacité à décrire les techniques de data visualisations existantes et il/elle est capable de les lister<br>-Il/Elle utilise l’outil datavisualisation de manière appropriée<br>-Il/Elle génère les rapports interactifs à l’aide de l’outil                                                                                                                                                                                                                                                |                                                                                                                    |
| 6 | Définir les données de référence de l’entreprise à partir des données utilisées pour créer un référentiel de données afin d’assurer la mise à disposition de données cohérentes aux directions métiers                                                        | Le candidat/La candidate est capable d’identifier le référentiel de données de l’entreprise :<br>- Il/Elle identifie et formalise par écrit des critères de sélection et de validation des données existantes                                                                                                                                                                                                                                                                                                                                                                                                |                                                                                                                    |
| 7 | Créer un entrepôt unique à partir du référentiel de données établi pour centraliser les informations stratégiques de l’entreprise et répondre rapidement aux besoins métiers                                                                                  | Le candidat/La candidate conçoit et argumente le choix du modèle multidimensionnel :<br>-En étoile, en flocon et grappe<br>-Il/Elle déploie son modèle multidimensionnel, (d’un entrepôt ou d’un cube), dans une solution BI (Business Intelligence)                                                                                                                                                                                                                                                                                                                                                         |                                                                                                                    |
| 8 | Assurer la qualité des données en utilisant les outils de gestion de la qualité de données pour garantir l’exactitude, la cohérence, la synchronisation et la traçabilité des données afin de satisfaire les besoins d’accessibilité des utilisateurs métiers | Le candidat/La candidate est capable de mesurer la qualité des données :<br>-Il/Elle est capable d’utiliser un outil traitant le nettoyage de données : (Data Cleansing, Data Quality Management)                                                                                                                                                                                                                                                                                                                                                                                                            |                                                                                                                    |
| 9 | Appliquer les procédures de sécurité établies par le / la RSSI de l’entreprise afin d’assurer la confidentialité et la sécurité des données et garantir une mise en conformité avec les obligations légales du RGPD                                           | Le candidat/La candidate propose une méthodologie de collecte de données :<br>-Respectant les aspects de la sécurité de données et les aspects juridiques (RGPD, clauses contractuelles client/fournisseur, propriété intellectuelle…)                                                                                                                                                                                                                                                                                                                                                                       |                                                                                                                    |

## Récupération du projet

Ce projet comporte des éléments de traitement de données et de machine learning, différents éléments d'architecture et de data / big data seront fournis depuis d'autres sources.

```shell
git clone
cd MSPR_I1_TPR813_DATA
python -m venv venv # 3.10
source venv/bin/activate
pip install -r requirements.txt
```
## Architecture simplifiée
```mermaid
graph TB
    subgraph API
        A[API] -->|Exec pipeline pour 'x'| P[Python]
        A -->|Train/Predict pour 'x'| D[ML_Booker]
    end
    subgraph Spark
        P -->|Exec sur Spark| S[Spark]
        S -->|Exec instructions| S
        D -->|Train| E[ML-training]
        E -->|Save| F[Model]
        D <-->|Predict| F
    end
    subgraph Data
        D -->|Save predictions| C
        S -->|Save Clean| C[DWH]
        C -->|Load data avec Spark| E
    end
```

## DATA

### Sources

#### Données electorales

On récupère les données electorales de Rennes Métropole sur l'api :
data: https://data.rennesmetropole.fr/api/explore/v2.1/catalog/datasets/resultats_elections_cumul_presidentielles_rennes/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B

Récupération des données des orientations politiques des cabinets politiques : 
wiki : https://fr.wikipedia.org/w/api.php?action=parse&page=Liste_de_partis_politiques_en_France&prop=wikitext&format=json

Récupération de l'affiliation aux cabinets politiques de chaque candidats (et donc de leur orientation):
wiki : https://fr.wikipedia.org/w/api.php?action=parse&page={$NOM_CANDIDAT}&prop=wikitext&format=json



##### Déclenchement du Pipeline (ETL) politique :
[pipeline.ipynb](src%2Fpolitical%2Fpipeline.ipynb)
```shell
# run Ipynb with jupyter and save the results to '--output'
python -m jupyter nbconvert --to notebook --execute src/political/pipeline.ipynb --output ../../pipeline_results.ipynb --import ../../src
```

Les données sont enregistrées dans un DATAWAREHOUSE (émulé avec sqlite), afin de faciliter les accès et les traitements.

##### Exemple d'utilisation des données résultantes :
```python
from src.political import conn
import pandas as pd

candidats = pd.read_sql_query("SELECT * FROM candidats", conn)
parties_politiques = pd.read_sql_query("SELECT * FROM parties", conn)
resultats_rennes = pd.read_sql_query("SELECT * FROM elections_rennes", conn)
```

#### Données climatiques

##### Déclenchement du Pipeline (ETL) weather :
[weather.ipynb](src%weather%weather.ipynb)
```shell
# run Ipynb with jupyter and save the results to '--output'
python -m jupyter nbconvert --to notebook --execute src/weather/weather.ipynb --output ../../weather_results.ipynb
```

#### Données insécurité

https://www.data.gouv.fr/fr/datasets/bases-statistiques-communale-et-departementale-de-la-delinquance-enregistree-par-la-police-et-la-gendarmerie-nationales/

Mise a jour du dataset : 19 juillet 2023

Mise a jour des résultats dans le README : 

Analyses des données brutes :

````shell
***** STATS *****
9898 Rows, 11 Cols (['classe', 'annee', 'Code.département', 'Code.région', 'unité.de.compte', 'millPOP', 'millLOG', 'faits', 'POP', 'LOG', 'tauxpourmille']
***** Nb col by type *****
int64 : 5
object : 6
***** Cols name by type *****
int64 : ['Code.région', 'millPOP', 'millLOG', 'faits', 'POP']
object : ['classe', 'annee', 'Code.département', 'unité.de.compte', 'LOG', 'tauxpourmille']
***** Unique values for object type column (limit 20) *****
classe : [['Coups et blessures volontaires', 'Coups et blessures volontaires intrafamiliaux', 'Autres coups et blessures volontaires', 'Violences sexuelles', 'Vols avec armes', 'Vols violents sans arme', 'Vols sans violence contre des personnes', 'Cambriolages de logement', 'Vols de véhicules', 'Vols dans les véhicules', "Vols d'accessoires sur véhicules", 'Destructions et dégradations volontaires', 'Trafic de stupéfiants', 'Usage de stupéfiants']]
annee : [['16', '17', '18', '19', '20', '21', '22']]
Code.département : [101]
unité.de.compte : [['victime', 'infraction', 'victime entendue', 'véhicule', 'Mis en cause']]
LOG : [503]
tauxpourmille : [9836]
Ignored col for unique values : ['Code.région', 'millPOP', 'millLOG', 'faits', 'POP']
````

Apres selection des features :
````shell
***** ADVANCED STATS *****
9898 Rows, 4 Cols (['city', 'event', 'year', 'nb']
***** Nb col by type *****
object : 2
int64 : 2
***** Cols name by type *****
object : ['city', 'event']
int64 : ['year', 'nb']
***** Unique values for object type column (limit 20) *****
city : [101]
event : [['Coups et blessures volontaires', 'Coups et blessures volontaires intrafamiliaux', 'Autres coups et blessures volontaires', 'Violences sexuelles', 'Vols avec armes', 'Vols violents sans arme', 'Vols sans violence contre des personnes', 'Cambriolages de logement', 'Vols de véhicules', 'Vols dans les véhicules', "Vols d'accessoires sur véhicules", 'Destructions et dégradations volontaires', 'Trafic de stupéfiants', 'Usage de stupéfiants']]
Ignored col for unique values : ['year', 'nb']
***** Unique values for column 'annee' *****
[16 17 18 19 20 21 22]
````

Récupération, sommes et statistiques sur les infractions pour la ville de Rennes :

````shell
************** total_event_by_city_by_year : rennes 
+----+--------+--------+-------+
|    |   city |   year |    nb |
|----+--------+--------+-------|
|  0 |     35 |     16 | 33056 |
|  1 |     35 |     17 | 34783 |
|  2 |     35 |     18 | 35286 |
|  3 |     35 |     19 | 34493 |
|  4 |     35 |     20 | 32271 |
|  5 |     35 |     21 | 33078 |
|  6 |     35 |     22 | 41119 |
+----+--------+--------+-------+

************** total_event_by_type_by_year : rennes 
+----+--------+----------------------------+----------------------------------+--------------------------------------------+-------------------------+
|    |   year |   Cambriolages de logement |   Coups et blessures volontaires |   Destructions et dégradations volontaires |   Trafic de stupéfiants |
|----+--------+----------------------------+----------------------------------+--------------------------------------------+-------------------------|
|  0 |     16 |                       1739 |                             2637 |                                       8400 |                     428 |
|  1 |     17 |                       2434 |                             2884 |                                       8568 |                     541 |
|  2 |     18 |                       2297 |                             2851 |                                       8384 |                     590 |
|  3 |     19 |                       2066 |                             3246 |                                       8302 |                     580 |
|  4 |     20 |                       1675 |                             3795 |                                       7407 |                     556 |
|  5 |     21 |                       1769 |                             3933 |                                       7228 |                     765 |
|  6 |     22 |                       2651 |                             4591 |                                       7867 |                     709 |
+----+--------+----------------------------+----------------------------------+--------------------------------------------+-------------------------+

2297 Cambriolages de logement year 2018 city 35
Les Cambriolages de logement en 2018 a rennes représentent 0.966% des chiffres nationaux
6.5096% de tous les events sont des Cambriolages de logement à Rennes en 2018
````


## MACHINE LEARNING

### SVM
- Features : [insécurité, chomage]
- Target : [resultat_election_precedente]