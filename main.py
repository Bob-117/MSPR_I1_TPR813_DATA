import pandas as pd

from src.data_extract import find_file
from src.data_transform import print_advanced_stats, print_dataset
from src.insecurity import create_insecurity_dataset, insecurity_stats, create_data_for_one_city, handle_one_city

BASE_DATA_SRC = 'data'

insecurity_file_departement = 'donnee-dep-data.gouv-2022-geographie2023-produit-le2023-07-17.csv'
insecurity_raw_file_departement = f'raw/csv/{insecurity_file_departement}'
insecurity_features_file_departement = 'infractions_departement_annee.csv'


# insecurity_file = 'donnee-data.gouv-2022-geographie2023-produit-le2023-07-17.csv'
# insecurity_raw_file = f'raw/csv/{insecurity_file}'
# insecurity_features_file_departement = 'infractions_natio_annee.csv'


def insecurity_process():
    print('============== RAW ==============')
    raw_file = find_file(insecurity_raw_file_departement)
    raw_df = pd.read_csv(raw_file, sep=';')
    print_advanced_stats(raw_df)
    print('============== TRANSFORM ==============')
    print('todo nan outliers')
    print('============== FEATURES ==============')
    # exit(117)
    # create_insecurity_dataset(insecurity_raw_file_departement)
    create_insecurity_dataset(
        _file_in=insecurity_raw_file_departement, _file_out='clean/insecurity/infractions_departement_annee.csv', save=True
    )
    features_file = find_file(insecurity_features_file_departement, target_dir='clean/insecurity')
    features_df = pd.read_csv(features_file)
    print_advanced_stats(features_df)
    insecurity_stats(features_df)
    # debug(features_df)
    print('============== CITY (35) ==============')
    handle_one_city(_d=features_df, city='35')


# insecurity_f = find_file(insecurity_features_file)
# insecurity_d = pd.read_csv(insecurity_f)
# print_advanced_stats(insecurity_d)
# insecurity_stats(insecurity_d)
# debug(insecurity_d)

if __name__ == '__main__':
    insecurity_process()
