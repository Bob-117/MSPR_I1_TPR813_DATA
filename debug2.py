import pandas as pd

from src.data_transform import print_dataset

d = pd.read_csv('data/raw/csv/infractions_departement_annee_debug.csv')

specific_event = 'Coups et blessures volontaires'

for year in d['year'].unique():
    df_year = d[d['year'] == year]

    # total number of events for the specific event in all cities for the current year
    total_events_current_year = df_year[df_year['event'] == specific_event]['nb'].sum()

    # percentage of the specific event for each city
    df_year['percentage'] = df_year.apply(lambda row: (row['nb'] / total_events_current_year) * 100 if row['event'] == specific_event else 0, axis=1)

    # avg percentage for each city for the current year
    avg_percentage_by_city = df_year.groupby('city')['percentage'].mean()

    # rescale to 10 todo round
    max_percentage = avg_percentage_by_city.max()
    min_percentage = avg_percentage_by_city.min()
    avg_percentage_by_city_normalized = 10 * (avg_percentage_by_city - min_percentage) / (max_percentage - min_percentage)

    d.loc[d['year'] == year, '%'] = d[d['year'] == year]['city'].map(avg_percentage_by_city_normalized)

print(d[['city', 'year', 'qualification']])
print_dataset(d)
#
# whole_cities = 237608
# whole_rennes = 35286
# part = 2297
#
# percentage = (part / whole_rennes) * 100
#
# print(percentage)