import pandas as pd
from src.data_transform import print_dataset, print_advanced_stats

d = pd.read_csv('data/raw/csv/infractions_departement_annee_debug.csv')


def total_event_by_type_by_year(_d):
    event_sum_per_year = _d.groupby(['year', 'event'])['nb'].sum().reset_index()
    event_sum_per_year_pivot = event_sum_per_year.pivot(index='year', columns='event', values='nb').reset_index()
    # event_sum_per_year_pivot = event_sum_per_year_pivot.fillna(0)
    return event_sum_per_year_pivot


def total_event_by_city_by_year():
    total_events_per_city = d.groupby('city')['nb'].sum().reset_index()
    return total_events_per_city


exit(117)


def rows(d):
    num_rows, _ = d.shape
    return num_rows


# print_dataset(d)
cities = d['city'].unique()
years = d['year'].unique()
events = d['event'].unique()

for city in cities:
    city_df = d[d['city'] == city]
    # print(rows(city_df))
    for y in years:
        city_year_df = city_df[city_df['year'] == y]
        if city == '35':
            print_dataset(city_year_df)
        # city_counts = city_year_df['event'].value_counts()
        # print(city_counts)
    # print(city_df)
