import os
import pytest

import logging

from src.data_extract import find_file

logging.basicConfig(level=logging.DEBUG)


@pytest.fixture
def files():
    return [
        (['raw', 'csv'], 'raw.csv'),
        (['clean'], 'clean.csv'),
        ([], 'datagouv.csv'),
    ]


# @pytest.fixture
# def test_data_dir(tmpdir, files, monkeypatch):
#     """
#     Fixture to create a temporary directory with test files.
#     """
#     data_dir = tmpdir.mkdir('test_data').join('data').strpath
#     monkeypatch.setenv('BASE_PROJECT', tmpdir.strpath)
#     monkeypatch.setenv('BASE_DATA_PATH', 'data')
#     monkeypatch.setenv('BASE_DATA_FILE_PATH', data_dir)
#
#     for dirs, file_name in files:
#         # sub_path = os.path.join(*dirs, file_name)
#         # file_path = data_dir.join(sub_path)
#         file_path = data_dir.join(*dirs + [file_name])
#         file_path.ensure(file=True, dir=True)
#         file_path.write('test data')
#
#     return data_dir


@pytest.fixture
def test_data_dir(tmpdir, files, monkeypatch):
    """

    """
    data_dir = tmpdir.mkdir('test_data').join('data')
    monkeypatch.setenv('BASE_PROJECT', tmpdir.strpath)
    monkeypatch.setenv('BASE_DATA_PATH', 'data')
    monkeypatch.setenv('BASE_DATA_FILE_PATH', data_dir.strpath)

    for dirs, file_name in files:
        dirs_path = data_dir.join(*dirs)
        dirs_path.ensure(dir=True)
        file_path = dirs_path.join(file_name)
        file_path.ensure(file=True)
        file_path.write('test data')

    return data_dir.strpath


@pytest.mark.parametrize(
    'file, directory, expected',
    [
        ('raw.csv', 'raw/csv', 'found'),
        ('clean.csv', 'clean', 'found'),
        ('datagouv.csv', None, 'found'),
        ('nope.csv', None, FileNotFoundError),
    ]
)
def test_file_ok(file, directory, expected, test_data_dir, monkeypatch):
    monkeypatch.setenv('BASE_PROJECT', test_data_dir)
    monkeypatch.setenv('BASE_DATA_PATH', 'data')
    monkeypatch.setenv('BASE_DATA_FILE_PATH', test_data_dir)

    if expected == 'found':
        if directory:
            f = find_file(file_name=file, target_dir=directory)
        else:
            f = find_file(file_name=file)
        assert f == f'Found {file} in {directory}'
    else:
        with pytest.raises(expected) as exc:
            v = exc.value
        assert v == 'NOPE'
